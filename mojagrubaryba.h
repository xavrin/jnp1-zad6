#ifndef MOJA_GRUBA_RYBA_H
#define MOJA_GRUBA_RYBA_H

#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <vector>
#include <sstream>
#include "grubaryba.h"

// We'll set all the variables important for the game to initial
// values before each game.

typedef unsigned int Money;

// It's for a flexibility of changing the way of rolling the dice.
class SetOfDice {
    public:
        SetOfDice() {}
        void setDice(std::shared_ptr<Die> die) { this->die = die; };
        bool is_empty() const { return !die; }
        unsigned short getResult() const { return die->roll() + die->roll(); }
    private:
        std::shared_ptr<Die> die;
};

class PlayersInterface;

class Field {
    public:
        Field(const std::string &name) : name(name) {}
        virtual void stop(PlayersInterface &player) {};
        virtual void pass(PlayersInterface &player) {};
        virtual void reset() {};
        std::string getName() const { return name; }
    protected:
        const std::string name;
};

class StartField : public Field {
    public:
        StartField(const std::string &name) : Field(name) {}
        virtual void stop(PlayersInterface &player);
        virtual void pass(PlayersInterface &player);
    private:
        const static Money reward = 50;
};

class RewardField : public Field {
    public:
        RewardField(const std::string &name, Money reward)
            : Field(name), reward(reward) {}
        virtual void stop(PlayersInterface &player);
    private:
        const Money reward;
};

class PenaltyField : public Field {
    public:
        PenaltyField(const std::string &name, Money penalty)
            : Field(name), penalty(penalty) {}
        virtual void stop(PlayersInterface &player);
    private:
        const Money penalty;
};

class DepositField : public Field {
    public:
        DepositField(const std::string &name, Money cost_per_passing)
            : Field(name), aggregated_money(0),
              cost_per_passing(cost_per_passing) {}
        virtual void stop(PlayersInterface &player);
        virtual void pass(PlayersInterface &player);
        virtual void reset();
    private:
        Money aggregated_money;
        const Money cost_per_passing;
};

class AquariumField : public Field {
    public:
        AquariumField(const std::string &name, unsigned short penalty_turns)
            : Field(name), penalty_turns(penalty_turns) {}
        virtual void stop(PlayersInterface &player);
    private:
        const unsigned short penalty_turns;
};

// PropertyField will not it's owner, to make actions easier
class PropertyField : public Field,
                      public std::enable_shared_from_this<PropertyField> {
    public:
        PropertyField(const std::string &name, Money buy_price,
                      Money stopping_fee)
            : Field(name), buy_price(buy_price), stopping_fee(stopping_fee),
              owner(NULL) {}
        virtual void stop(PlayersInterface &player);
        void sell();
        virtual void reset();
        Money getBuyPrice() const { return buy_price; }
    protected:
        const static unsigned int SELL_PERCENTAGE = 50;
        const Money buy_price, stopping_fee;
        PlayersInterface* owner;
        Money getSellPrice() const {
            return SELL_PERCENTAGE * buy_price / 100;
        }
};

class CoralPropertyField : public PropertyField {
    public:
        CoralPropertyField(const std::string &name, Money buy_price)
            : PropertyField(name, buy_price,
                            buy_price * FEE_PERCENTAGE / 100) {}
    private:
        const static unsigned int FEE_PERCENTAGE = 20;
};

class PublicInstitutionPropertyField : public PropertyField {
    public:
        PublicInstitutionPropertyField(const std::string &name,
                                       Money buy_price)
            : PropertyField(name, buy_price,
                            buy_price * FEE_PERCENTAGE / 100) {}
    private:
        const static unsigned int FEE_PERCENTAGE = 40;
};

// Board will contain all the fields.
// The Piece class will behave as a sort of 'iterator' of Board,
// to make playing easier and to hide implementation of Board.
class Board {
    public:
        class Piece {
            public:
                Piece(Board &board) : board(board) {}
                void setAtStart();
                const std::shared_ptr<Field>& getField() const { return *it; }
                void moveForward();
            private:
                friend class Board;
                std::vector<std::shared_ptr<Field> >::iterator it;
                Board& board;
        };
        Board();
        Piece getPiece() { return Piece(*this); }
        void reset();
    private:
        friend class Piece;
        std::vector<std::shared_ptr<Field> > fields;
};

// It's an iterface of operations provided from Player to Fields.
class PlayersInterface {
    public:
        virtual void receiveMoney(Money amount) = 0;
        virtual Money payFee(Money amount) = 0;
        virtual void setPenalty(unsigned int penalty) = 0;
        virtual bool doYouBuy(const std::shared_ptr<PropertyField> &field) = 0;
};

// It's worth noticing that PlayersInterface is inherited in protected way, so that
// nobody from 'the outside' will make that operations on player.
class Player : protected PlayersInterface {
    public:
        typedef std::list<std::shared_ptr<PropertyField> > Properties;
        Player(const Board::Piece &piece, const std::string &name,
                Money starting_money)
            : name(name), piece(piece), starting_money(starting_money),
              penalty(0), bankrupt(false) {}
        virtual bool wantBuy(const Field &field) const = 0;
        virtual bool wantSell(const Field &field) const = 0;
        bool isBankrupt() const { return bankrupt; }
        // description of situation of the player
        std::string getDescription() const;
        virtual void reset();
        // returns whether player is still in game
        bool playTurn(const SetOfDice &dice);
    protected:
        std::string name;
        Board::Piece piece;
        // money player gets at start
        const Money starting_money;
        unsigned int penalty; // nr of turns player will skip
        bool bankrupt;
        Properties properties;
        Money money;
        void receiveMoney(Money amount);
        virtual void setPenalty(unsigned int _penalty) { penalty = _penalty; }
        Money payFee(Money amount);
        void askToSellAllProperties();
        // returns amount of player's money after selling all of his properties
        Money goBankrupt();
        // return whether player bought the property
        virtual bool doYouBuy(const std::shared_ptr<PropertyField> &field);
};

// It's a wrapper for Human class that satisfy the interface of Player class.
class HumanPlayer : public Player {
    public:
        // assumming that human is a correct pointer
        HumanPlayer(const Board::Piece &piece,
                    const std::shared_ptr<Human> &human,
                    Money starting_money)
            : Player(piece, human->getName(), starting_money), human(human) {}
        virtual bool wantBuy(const Field &field) const;
        virtual bool wantSell(const Field &field) const;
    private:
        std::shared_ptr<Human> human;
};

// It's a sort of ComputerPlayer's engine that's responsible for decisions.
class ComputerLogic {
    public:
        virtual bool wantBuy(const Field &field) = 0;
        virtual bool wantSell(const Field &field) { return false; }
        virtual void reset() {};
};

class DumbLogic : public ComputerLogic {
    public:
        DumbLogic() : counter(0) {}
        virtual bool wantBuy(const Field &field);
        virtual void reset() { counter = 0; }
    private:
        const static unsigned int HOW_FREQUENT = 3;
        unsigned int counter;
};

class SmartassLogic : public ComputerLogic {
    public:
        virtual bool wantBuy(const Field &field);
};

class ComputerPlayer : public Player {
    public:
        ComputerPlayer(const Board::Piece &piece,
                       std::unique_ptr<ComputerLogic> logic,
                       const std::string &name,
                       Money starting_money)
            : Player(piece, name, starting_money), logic(std::move(logic)) {}
        virtual bool wantBuy(const Field &field) const;
        virtual bool wantSell(const Field &field) const;
        virtual void reset() { Player::reset(); logic->reset(); }
    private:
        std::unique_ptr<ComputerLogic> logic;
};

class MojaGrubaRyba : public GrubaRyba {
    public:
        MojaGrubaRyba() {};
        virtual ~MojaGrubaRyba() {}
        virtual void setDie(std::shared_ptr<Die> die);
        virtual void addComputerPlayer(GrubaRyba::ComputerLevel level);
        virtual void addHumanPlayer(std::shared_ptr<Human> human);
        virtual void play(unsigned int rounds);
    private:
        const static size_t MIN_NUM_OF_PLAYERS = 2,
                            MAX_NUM_OF_PLAYERS = 8;
        const static Money STARTING_MONEY = 1000;
        Board board;
        SetOfDice dice;
        unsigned int active_players;
        std::list<std::shared_ptr<Player> > players;
        void playRound();
        void describeRound(unsigned int round) const;
        std::string getNextName() const;
        void reset();
        std::unique_ptr<ComputerLogic>
        getLogic(const ComputerLevel level) const;
};

#endif // MOJA_GRUBA_RYBA_H
