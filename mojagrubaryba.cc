#include "mojagrubaryba.h"

namespace {
    using std::string;
    using std::unique_ptr;
    using std::shared_ptr;
    using std::cout;
    using std::endl;
};

void StartField::stop(PlayersInterface &player) {
    player.receiveMoney(reward);
}

void StartField::pass(PlayersInterface &player) {
    player.receiveMoney(reward);
}

void RewardField::stop(PlayersInterface &player) {
    player.receiveMoney(reward);
}

void PenaltyField::stop(PlayersInterface &player) {
    player.payFee(penalty);
}

void DepositField::stop(PlayersInterface &player) {
    player.receiveMoney(aggregated_money);
    aggregated_money = 0;
}

void DepositField::pass(PlayersInterface &player) {
    aggregated_money += player.payFee(cost_per_passing);
}

void DepositField::reset() {
    aggregated_money = 0;
}

void AquariumField::stop(PlayersInterface &player) {
    player.setPenalty(penalty_turns);
}

void PropertyField::stop(PlayersInterface &player) {
    if (!owner) {
        if (player.doYouBuy(shared_from_this()))
            owner = &player;
    }
    else if (owner != &player)
        owner->receiveMoney(player.payFee(stopping_fee));
}

void PropertyField::sell() {
    owner->receiveMoney(getSellPrice());
    owner = NULL;
}

void PropertyField::reset() {
    owner = NULL;
}

Board::Board() {
    fields = {
        shared_ptr<StartField>
            (new StartField("Start")),
        shared_ptr<CoralPropertyField>
            (new CoralPropertyField("Anemonia", 160)),
        shared_ptr<Field>
            (new Field("Wyspa")),
        shared_ptr<CoralPropertyField>
            (new CoralPropertyField("Aporina", 220)),
        shared_ptr<AquariumField>
            (new AquariumField("Akwarium", 3)),
        shared_ptr<PublicInstitutionPropertyField>
            (new PublicInstitutionPropertyField("Grota", 300)),
        shared_ptr<CoralPropertyField>
            (new CoralPropertyField("Menella", 280)),
        shared_ptr<DepositField>
            (new DepositField("Laguna", 15)),
        shared_ptr<PublicInstitutionPropertyField>
            (new PublicInstitutionPropertyField("Statek", 250)),
        shared_ptr<RewardField>
            (new RewardField("Blazenki", 120)),
        shared_ptr<CoralPropertyField>
            (new CoralPropertyField("Pennatula", 400)),
        shared_ptr<PenaltyField>
            (new PenaltyField("Rekin", 180))
    };
}

void Board::reset() {
    for (auto& field_ptr : fields)
        field_ptr->reset();
}

void Board::Piece::setAtStart() {
    it = board.fields.begin();
}

void Board::Piece::moveForward() {
    it++;
    if (it == board.fields.end())
        setAtStart();
}

void Player::receiveMoney(Money amount) {
    money += amount;
}

Money Player::payFee(Money fee) {
    if (money < fee)
        askToSellAllProperties();
    if (money < fee)
        // we haven't gathered enough money = we go bankrupt
        return goBankrupt();
    money -= fee;
    return fee;
}

void Player::askToSellAllProperties() {
    Properties::iterator it = properties.begin();
    while (it != properties.end()) {
        if (wantSell(*(*it))) {
            (*it)->sell();
            it = properties.erase(it);
        }
        else it++;
    }
}

Money Player::goBankrupt() {
    for (const auto &property: properties)
        property->sell();
    bankrupt = true;
    return money;
}

bool Player::doYouBuy(const shared_ptr<PropertyField> &field) {
    if (!wantBuy(*field))
        return false;
    auto price = field->getBuyPrice();
    if (money < price)
        askToSellAllProperties();
    if (money < price)
        return false;
    money -= price;
    properties.push_back(field);
    return true;
}

void Player::reset() {
    bankrupt = false;
    penalty = 0;
    money = starting_money;
    piece.setAtStart();
    properties.clear();
}

bool Player::playTurn(const SetOfDice &dice) {
    if (penalty)
        if (--penalty)
            return true;
    auto number_of_moves = dice.getResult();
    for (unsigned short i = 0; i + 1 < number_of_moves; i++) {
        piece.moveForward();
        piece.getField()->pass(*this);
        if (isBankrupt())
            return false;
    }
    piece.moveForward();
    piece.getField()->stop(*this);
    return !isBankrupt();
}

string Player::getDescription() const {
    std::stringstream s;
    s << name;
    if (isBankrupt())
        s << " *** bankrut ***";
    else {
        s << " pole: " << piece.getField()->getName();
        if (penalty)
            s << " *** czekanie: " << penalty << " ***";
        else
            s << " gotowka: " << money;
    }
    return s.str();
}

bool HumanPlayer::wantBuy(const Field &field) const {
    return human->wantBuy(field.getName());
}

bool HumanPlayer::wantSell(const Field &field) const {
    return human->wantSell(field.getName());
}

bool DumbLogic::wantBuy(const Field &field) {
    return !(++counter % HOW_FREQUENT);
}

bool SmartassLogic::wantBuy(const Field &field) {
    return true;
}

bool ComputerPlayer::wantBuy(const Field &field) const {
    return logic->wantBuy(field);
}

bool ComputerPlayer::wantSell(const Field &field) const {
    return logic->wantSell(field);
}

void MojaGrubaRyba::playRound() {
    auto iter = players.begin();
    while (active_players > 1 && iter != players.end()) {
        auto player_ptr = *iter;
        if (!player_ptr->isBankrupt())
            if (!player_ptr->playTurn(dice))
                active_players--;
        iter++;
    }
}

void MojaGrubaRyba::setDie(shared_ptr<Die> die) {
    if (die)
        dice.setDice(die);
}

string MojaGrubaRyba::getNextName() const {
    return "Gracz" + std::to_string(players.size() + 1);
}

unique_ptr<ComputerLogic>
MojaGrubaRyba::getLogic(const ComputerLevel level) const {
    unique_ptr<ComputerLogic> ptr;
    switch (level) {
        case ComputerLevel::DUMB:
            ptr = unique_ptr<ComputerLogic>(new DumbLogic()); break;
        case ComputerLevel::SMARTASS:
            ptr = unique_ptr<ComputerLogic>(new SmartassLogic()); break;
    }
    return ptr;
}

void MojaGrubaRyba::addComputerPlayer(ComputerLevel level) {
    if (players.size() == MAX_NUM_OF_PLAYERS)
        throw TooManyPlayersException(MAX_NUM_OF_PLAYERS);
    shared_ptr<ComputerPlayer> ptr(
        new ComputerPlayer(board.getPiece(), getLogic(level),
                           getNextName(), STARTING_MONEY));
    players.push_back(ptr);
}

void MojaGrubaRyba::addHumanPlayer(shared_ptr<Human> human) {
    if (human) {
        if (players.size() == MAX_NUM_OF_PLAYERS)
            throw TooManyPlayersException(MAX_NUM_OF_PLAYERS);
        shared_ptr<HumanPlayer> ptr(
            new HumanPlayer(board.getPiece(), human, STARTING_MONEY));
        players.push_back(ptr);
    }
}

void MojaGrubaRyba::reset() {
    for (auto player_ptr: players)
        player_ptr->reset();
    active_players = players.size();
}

void MojaGrubaRyba::describeRound(unsigned int round) const {
    cout << "Runda: " << round << endl;
    for (const auto& ptr : players)
        cout << ptr->getDescription() << endl;
}

void MojaGrubaRyba::play(unsigned int rounds) {
    if (dice.is_empty())
        throw NoDieException();
    if (players.size() < MIN_NUM_OF_PLAYERS)
        throw TooFewPlayersException(MIN_NUM_OF_PLAYERS);
    reset();
    unsigned int round_count = 1;
    while (round_count <= rounds && active_players > 1) {
        playRound();
        describeRound(round_count++);
    }
}
