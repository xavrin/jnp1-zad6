CPP = g++
CPPFLAGS = -Wall -std=c++11

all: test0 test1

test0: test0.o mojagrubaryba.o
	$(CPP) $(CPPFLAGS) $^ -lm -o $@

test1: test1.o mojagrubaryba.o
	$(CPP) $(CPPFLAGS) $^ -lm -o $@

%.o: %.cc
	$(CPP) $(CPPFLAGS) -c $< -o $@

.PHONY: clean FAKE

clean:
	rm *.o
