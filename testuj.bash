#!/bin/bash
if ! make
then
    exit
fi
for i in test*.out
do
    name=${i/.out/}
    ./$name > m.out
    if diff m.out $i
    then
        echo "OK"
    else
        echo "FAIL"
    fi
    rm m.out
done
make clean
rm test0 test1
